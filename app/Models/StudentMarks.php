<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentMarks extends Model
{
    use HasFactory, SoftDeletes;


    public function student()
    {
        return $this->belongsTo(Students::class);
    }
}
