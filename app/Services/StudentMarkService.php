<?php

namespace App\Services;

use App\Models\StudentMarks;
use Exception;

class StudentMarkService
{
    //Student Mark Related Services
    public function saveMarks($request)
    {
        try {
            $studentMark = new StudentMarks();
            $studentMark->student_id = $request->student;
            $studentMark->term = $request->term;
            $studentMark->maths = $request->maths_mark;
            $studentMark->science = $request->science_mark;
            $studentMark->history = $request->history_mark;
            $studentMark->save();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateMarks($request, $id)
    {
        try {
            StudentMarks::where('id', $id)->update([
                'student_id' => $request->student,
                'term' => $request->term,
                'maths' => $request->maths_mark,
                'science' => $request->science_mark,
                'history' => $request->history_mark,
            ]);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteMark($id)
    {
        try {
            StudentMarks::destroy($id);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
