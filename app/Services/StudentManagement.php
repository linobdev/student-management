<?php

namespace App\Services;

use App\Models\Students;
use Exception;

class StudentManagement
{
    //Student Management Related Services
    public function saveStudent($studentInfo)
    {
        try {
            $student = new Students();
            $student->name = $studentInfo->name;
            $student->age = $studentInfo->age;
            $student->gender = $studentInfo->gender;
            $student->teacher_name = $studentInfo->teacher;
            $student->save();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateStudent($request, $id)
    {
        try {
            Students::where('id', $id)->update([
                'name' => $request->name,
                'age' => $request->age,
                'gender' => $request->gender,
                'teacher_name' => $request->teacher,
            ]);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function studentDelete($id)
    {
        try {
            Students::destroy($id);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
