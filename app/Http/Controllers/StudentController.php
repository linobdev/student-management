<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Students;
use App\Services\StudentManagement;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function __construct(StudentManagement $studentManagement)
    {
        $this->studentManagement = $studentManagement;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Students::query();

            return DataTables::of($data)
                ->addColumn('action', function ($row) {
                    $btn = '<a href="' . route('students.edit', $row->id) . '" ><i style="font-size:20px;cursor: pointer;" class="fa">&#xf044;</i></a>';
                    $btn .= '<form action="' . route('students.destroy', $row->id) . '" method="post" style="display: contents;"><input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="' . csrf_token() . '"><button type="submit" style="border:none;margin-left:4px;"><i style="font-size:20px;cursor: pointer;" class="fa">&#xf014;</i></button></form>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('students.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'age' => 'required|numeric',
            'gender' => 'required',
            'teacher' => 'required',
        ]);
        $studentSave = $this->studentManagement->saveStudent($request);

        if ($studentSave)
            return redirect('/students')->with('message', 'Student Added Successfully');
        return redirect('/students')->with('error', 'Something Went Wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Students::find($id);

        return view('students.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'age' => 'required|numeric',
            'gender' => 'required',
            'teacher' => 'required',
        ]);
        $studentUpdate = $this->studentManagement->updateStudent($request, $id);

        if ($studentUpdate)
            return redirect('/students')->with('message', 'Student Details Updated Successfully');
        return redirect('/students')->with('error', 'Something Went Wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $studentDelete = $this->studentManagement->studentDelete($id);

        if ($studentDelete)
            return redirect('/students')->with('message', 'Student Deleted Successfully');
        return redirect('/students')->with('error', 'Something Went Wrong');
    }
}
