<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\StudentMarks;
use App\Services\StudentMarkService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StudentMarkController extends Controller
{

    public function __construct(StudentMarkService $studentMarks)
    {
        $this->studentMarks = $studentMarks;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = StudentMarks::with('student');

            return DataTables::of($data)
                ->addColumn('total_marks', function ($row) {
                    $totalMarks = $row->maths + $row->history + $row->science;
                    return $totalMarks;
                })
                ->addColumn('created_on', function ($row) {
                    $time = Carbon::parse($row->created_at)->isoFormat('MMMM DD, YYYY hh:mm A');;
                    return $time;
                })
                ->orderColumn('created_on', function ($query, $order) {
                    $query->orderBy('created_at', $order);
                })
                ->addColumn('action', function ($row) {
                    $btn = '<a href="' . route('student-marks.edit', $row->id) . '" ><i style="font-size:20px;cursor: pointer;" class="fa">&#xf044;</i></a>';
                    $btn .= '<form action="' . route('student-marks.destroy', $row->id) . '" method="post" style="display: contents;"><input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="' . csrf_token() . '"><button type="submit" style="border:none;margin-left:4px;"><i style="font-size:20px;cursor: pointer;" class="fa">&#xf014;</i></button></form>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('marks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('marks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'student' => 'required',
            'term' => 'required',
            'maths_mark' => 'required|numeric|max:50',
            'science_mark' => 'required|numeric|max:50',
            'history_mark' => 'required|numeric|max:50',
        ]);

        $storeData = $this->studentMarks->saveMarks($request);
        if ($storeData)
            return redirect('/student-marks')->with('message', 'Student Marks Saved');
        return redirect('/student-marks')->with('error', 'Something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $studentMarks = StudentMarks::with('student')->find($id);

        return view('marks.edit', compact('studentMarks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'student' => 'required',
            'term' => 'required',
            'maths_mark' => 'required|numeric|max:50',
            'science_mark' => 'required|numeric|max:50',
            'history_mark' => 'required|numeric|max:50',
        ]);

        $updateData = $this->studentMarks->updateMarks($request, $id);
        if ($updateData)
            return redirect('/student-marks')->with('message', 'Student Marks Updated Successfully');
        return redirect('/student-marks')->with('error', 'Something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteMark = $this->studentMarks->deleteMark($id);;

        if ($deleteMark)
            return redirect('/student-marks')->with('message', 'Student Marks Deleted Successfully');
        return redirect('/student-marks')->with('error', 'Something went wrong');
    }
}
