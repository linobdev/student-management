(function($) {
  'use strict'; 
  $(function() {

    //Revenue Chart
    if ($("#revenue-chart").length) {

      var  max_roomCount = Math.max.apply(Math, roomCount);

      var step_interval = (max_roomCount)/5;
      step_interval = Math.ceil(step_interval/5)*5;
      max_roomCount = Math.ceil(max_roomCount/step_interval)*step_interval;

      var len = roomLabels.length,
                i, j, stop;

        for (i=0; i < len; i++){
            for (j=0, stop=len-i; j < stop; j++){
                if (roomCount[j] < roomCount[j+1]){
                    swap(roomCount,roomLabels, j, j+1);
                }
            }
        }
        roomCount = roomCount.slice(0, 5);
        roomLabels = roomLabels.slice(0, 5);


      var galleryVisitorDeviceChartCanvas = $("#revenue-chart").get(0).getContext("2d");


      var galleryVisitorDeviceChart = new Chart(galleryVisitorDeviceChartCanvas, {
        type: 'horizontalBar',
        data: {
            labels:roomLabels, //["JCP", "Belk", "Hudson", "Sports", "Cafe"],
            datasets: [{
                data:roomCount, //[1000, 1170, 660, 1030, 1200],
                backgroundColor: "#AF7AC5",
              }
              ]
            },
            options: {
              animation: {
            duration: 1,
            onComplete: function () {
                var chartInstance = this.chart,
                    ctx = chartInstance.ctx;
                    ctx.textAlign = 'center';
                    ctx.fillStyle = "rgba(0, 0, 0, 1)";
                    ctx.textBaseline = 'bottom';
                    // Loop through each data in the datasets
                    this.data.datasets.forEach(function (dataset, i) {  
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x+8, bar._model.y +8);
                        });
                    });
                }
            },
              responsive: true,
              maintainAspectRatio: false,
              scales: {
                yAxes: [{
                  gridLines: {
                    drawBorder: false,
                    zeroLineColor: "rgba(0, 0, 0, 0.09)",
                    color: "rgba(0, 0, 0, 0.09)"
                  },
                  ticks: {
                    fontColor: '#000000',
                    min:0,
                    stepSize: step_interval,
                    min: 0,
                    max: max_roomCount
                  }
                }],
                xAxes: [{
                  ticks: {
                    fontColor: '#000000',
                    beginAtZero: true,
                    maxRotation: 90,
                    minRotation: 0
                  },
                  gridLines: {
                    display: false,
                    drawBorder: false
                  },
                  barPercentage: 0.4
                }]
              },
              legend: {
                display: false
              },
              plugins: {
                labels: false
              }
            }
          });
    }
    //Sales Chart
    if ($("#chart-sales").length) {

      var  max_hourlyCount = Math.max.apply(Math, hourlyCount);
      var step_interval = max_hourlyCount/5;
      step_interval = Math.ceil(step_interval/5)*5;
      max_hourlyCount = Math.ceil(max_hourlyCount/step_interval)*step_interval;
      var sortedHourlyCount = [];
      var labelForGraph = [];
      var j = 0;

      var combinedData =  hourlyCount.reduce(function(combinedData, field, index) {
        combinedData[hourlyTimes[index]] = field;
        return combinedData;
      }, {});


      hourlyTimes.sort(function (a, b) {
        return new Date('1970/01/01 ' + a) - new Date('1970/01/01 ' + b);
      });

      for (var i in hourlyTimes) {
        sortedHourlyCount.push(combinedData[hourlyTimes[i]]);
      }

      for (var i in hourlyTimes) {
          j = parseInt(i) + 1;
         
          if (typeof hourlyTimes[j] != "undefined") {

              labelForGraph.push(hourlyTimes[i] + ' - '+hourlyTimes[j]);
          }
          else
              labelForGraph.push('After ' + hourlyTimes[i]);

      }

      var hourlyVisitorDeviceChartCanvas = $("#chart-sales").get(0).getContext("2d");
      var gradient1 = hourlyVisitorDeviceChartCanvas.createLinearGradient(0, 0, 0, 230);
      gradient1.addColorStop(0, '#55d1e8');
      gradient1.addColorStop(1, 'rgba(255, 255, 255, 0)');

      var gradient2 = hourlyVisitorDeviceChartCanvas.createLinearGradient(0, 0, 0, 160);
      gradient2.addColorStop(0, '#1bbd88');
      gradient2.addColorStop(1, 'rgba(255, 255, 255, 0)');

      var hourlyVisitorDeviceChart = new Chart(hourlyVisitorDeviceChartCanvas, {
        type: 'bar',
        data: {
            labels:labelForGraph, //["JCP", "Belk", "Hudson", "Sports", "Cafe"],
            datasets: [{
                data:sortedHourlyCount, //[1000, 1170, 660, 1030, 1200],
                backgroundColor: "#AF7AC5",
                
              }
              ]
            },
            options: {
              animation: {
            duration: 1,
            onComplete: function () {
                var chartInstance = this.chart,
                    ctx = chartInstance.ctx;
                    ctx.textAlign = 'center';
                    ctx.fillStyle = "rgba(0, 0, 0, 1)";
                    ctx.textBaseline = 'bottom';
                    // Loop through each data in the datasets
                    this.data.datasets.forEach(function (dataset, i) {  
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y );
                        });
                    });
                }
            },
              responsive: true,
              maintainAspectRatio: true,
              plugins: {
                filler: {
                  propagate: false
                }
              },
              scales: {
                yAxes: [{
                  gridLines: {
                    drawBorder: false,
                    zeroLineColor: "rgba(0, 0, 0, 0.09)",
                    color: "rgba(0, 0, 0, 0.09)"
                  },
                  ticks: {
                    fontColor: '#000000',
                    min:0,
                    stepSize: step_interval,
                    min: 0,
                    max: max_hourlyCount
                  }
                }],
                xAxes: [{
                  ticks: {
                    fontColor: '#000000',
                    beginAtZero: true,
                    maxRotation: 90,
                    minRotation: 70
                  },
                  gridLines: {
                    display: false,
                    drawBorder: false
                  },
                  barPercentage: 0.4
                }]
              },
              legend: {
                display: false
              },
              plugins: {
                labels: false
              }
            }
          });
    }
    
    

  });
})(jQuery);
function swap(arr1, arr2, first_Index, second_Index){
    var temp1 = arr1[first_Index];
    arr1[first_Index] = arr1[second_Index];
    arr1[second_Index] = temp1;
    var temp2 = arr2[first_Index];
    arr2[first_Index] = arr2[second_Index];
    arr2[second_Index] = temp2;
} 


