# Student Management System

Laravel Version Used:- 8.83.23

## Getting started

Steps to be followed for setting up

1. Clone the project from the shared gitlab url to the desired folder depending upon the server using.
2. After cloning run the commmand "composer install"
3. Create the database and enter the database name, password and username in the env file
4. Run the migration command "php artisan:migrate"
