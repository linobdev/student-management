<nav class="mdc-list mdc-drawer-menu">

    <div class="mdc-list-item mdc-drawer-item">
        <a class="mdc-drawer-link" href="{{route('students.index')}}">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">description</i>
            Students
        </a>
    </div>

    <div class="mdc-list-item mdc-drawer-item">
        <a class="mdc-drawer-link" href="{{route('student-marks.index')}}">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">receipt</i>
            Student Mark List
        </a>
    </div>

</nav>