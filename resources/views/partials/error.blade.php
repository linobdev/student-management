@if (Session::has('error'))
<div class="alert alert-success">
    <strong>Error!</strong> {!! session('error') !!}
</div>
@endif