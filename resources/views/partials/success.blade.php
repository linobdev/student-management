@if (Session::has('message'))
<div class="alert alert-success">
    <strong>Success!</strong> {!! session('message') !!}
</div>
@endif