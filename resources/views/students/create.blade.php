@extends('layouts.app')

@section('section-name', 'Add Student')

@section('content')
<div class="mdc-layout-grid">
    <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-12-desktop">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{route('students.store')}}" method="post" id="addStudent" class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-desktop">
                <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
                    <p style="font-size: 15px; float: right; color:rgba(94, 94, 94, 0.87);"></p>
                    {{csrf_field()}}
                </div>
                <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-12-desktop">
                    <div class="mdc-card">
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
                            <p style="font-size: 15px; float: right; color:rgba(94, 94, 94, 0.87);">Add Student</p>
                        </div>
                        <br>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12-desktop">
                            <div class="mdc-text-field mdc-text-field--outlined">
                                <input class="mdc-text-field__input" id="name" type="text" name="name" value="{{ old('name') }}" required>
                                <div class="mdc-notched-outline">
                                    <div class="mdc-notched-outline__leading"></div>
                                    <div class="mdc-notched-outline__notch">
                                        <label for="text-field-hero-input" class="mdc-floating-label">Enter Student Name</label>
                                    </div>
                                    <div class="mdc-notched-outline__trailing"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12-desktop">
                            <div class="mdc-text-field mdc-text-field--outlined">
                                <input class="mdc-text-field__input" id="age" type="text" name="age" value="{{ old('age') }}" maxlength="2" required onkeypress="return isNumberKey(event)" />
                                <div class="mdc-notched-outline">
                                    <div class="mdc-notched-outline__leading"></div>
                                    <div class="mdc-notched-outline__notch">
                                        <label for="text-field-hero-input" class="mdc-floating-label">Enter Student Age</label>
                                    </div>
                                    <div class="mdc-notched-outline__trailing"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12-desktop">
                            <div class="mdc-text-field mdc-text-field--outlined ">
                                <select class="mdc-text-field__input" name="gender" id="gender" required>
                                    <option value=" ">Select</option>
                                    <option value="M" @if(old('gender')=="M" ) selected @endif>Male</option>
                                    <option value="F" @if(old('gender')=="F" ) selected @endif>Female</option>
                                </select>
                                <div class="mdc-notched-outline">
                                    <div class="mdc-notched-outline__leading"></div>
                                    <div class="mdc-notched-outline__notch">
                                        <label for="text-field-hero-input" class="mdc-floating-label">Select Gender</label>
                                    </div>
                                    <div class="mdc-notched-outline__trailing"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12-desktop">
                            <div class="mdc-text-field mdc-text-field--outlined ">
                                <select class="mdc-text-field__input" name="teacher" id="teacher" required>
                                    <option value=" ">Select</option>
                                    <option value="Katie" @if(old('teacher')=="Katie" ) selected @endif>Katie</option>
                                    <option value="Max" @if(old('teacher')=="Max" ) selected @endif>Max</option>
                                </select>
                                <div class="mdc-notched-outline">
                                    <div class="mdc-notched-outline__leading"></div>
                                    <div class="mdc-notched-outline__notch">
                                        <label for="text-field-hero-input" class="mdc-floating-label">Select Reporting Teacher</label>
                                    </div>
                                    <div class="mdc-notched-outline__trailing"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
                            <button type="submit" class="mdc-button mdc-button--raised w-100">Add Student</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('append-assets')
<script>
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
@endsection