@extends('layouts.app')

@section('section-name', 'Student Management')

@section('prepend-assets')
<link rel="stylesheet" type="text/css" href="{{asset('/css/datatables.min.css')}}" />
@endsection

@section('content')
@include('partials.success')
@include('partials.error')
<div class="mdc-layout-grid__inner">
    <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
        <div class="mdc-card">
            <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6" style="margin-bottom: 10px;">
                <a href="{{route('students.create')}}"><button class="mdc-button mdc-button--raised w-100 studentAdd">Add Student</button></a>
            </div>
            <table id="studentTable" class="table table-striped table-bordered hats-datatable" style="width:100%;margin-top:40px;">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>Reporting Teacher</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection


@section('append-assets')
<script type="text/javascript" src="{{asset('/js/datatables.min.js')}}"></script>
<script>
    $(document).ready(function() {
        var table = $('#studentTable').dataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('students.index') }}",
            columns: [{
                    data: 'id',
                    name: 'id',
                    searchable: false,
                    orderable: false,
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'age',
                    name: 'age'
                },
                {
                    data: 'gender',
                    name: 'gender'
                },
                {
                    data: 'teacher_name',
                    name: 'teacher_name'
                },
                {
                    data: 'action',
                    name: 'action',
                    searchable: false,
                    orderable: false,
                },
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $("td:first", nRow).html(iDisplayIndex + 1);
                return nRow;
            },
        });
    });
</script>

@endsection