<!DOCTYPE html>
<html lang="en">
@include('partials.header')

<body>
    <div class="body-wrapper">
        <aside class="mdc-drawer mdc-drawer--dismissible mdc-drawer--open">
            <div class="mdc-drawer__header">
                <a href="{{ url('/') }}" class="brand-logo">
                    <h2 style="color:#fff;">{{ config('app.name') }}</h2>
                </a>
            </div>
            <div class="mdc-drawer__content">
                <div class="mdc-list-group">
                    @include('partials.left-nav')
                </div>
            </div>
        </aside>

        <div class="main-wrapper mdc-drawer-app-content">
            <header class="mdc-top-app-bar">
                <div class="mdc-top-app-bar__row">
                    <div class="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
                        <button class="material-icons mdc-top-app-bar__navigation-icon mdc-icon-button sidebar-toggler">menu</button>
                        <span class="mdc-top-app-bar__title">@yield('section-name')</span>
                    </div>
                </div>
            </header>

            <div class="page-wrapper mdc-toolbar-fixed-adjust">
                <main class="content-wrapper">
                    <div class="mdc-layout-grid">
                        @yield('content')
                    </div>
                </main>
            </div>
        </div>
    </div>
    @include('partials.scripts')
</body>

</html>