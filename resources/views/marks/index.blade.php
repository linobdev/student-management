@extends('layouts.app')

@section('section-name', 'Student Mark List')

@section('prepend-assets')
<link rel="stylesheet" type="text/css" href="{{asset('/css/datatables.min.css')}}" />
@endsection

@section('content')
@include('partials.success')
@include('partials.error')
<div class="mdc-layout-grid__inner">
    <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
        <div class="mdc-card">
            <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6" style="margin-bottom: 10px;">
                <a href="{{route('student-marks.create')}}"><button class="mdc-button mdc-button--raised w-100 studentAdd">Add Student Marks</button></a>
            </div>
            <table id="studentMarks" class="table table-striped table-bordered hats-datatable" style="width:100%;margin-top:40px;">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Maths</th>
                        <th>Science</th>
                        <th>History</th>
                        <th>Term</th>
                        <th>Total Marks</th>
                        <th>Created On</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection


@section('append-assets')
<script type="text/javascript" src="{{asset('/js/datatables.min.js')}}"></script>
<script>
    $(document).ready(function() {
        var table = $('#studentMarks').dataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('student-marks.index') }}",
            columns: [{
                    data: 'id',
                    name: 'id',
                    searchable: false,
                    orderable: false,
                },
                {
                    data: 'student.name',
                    name: 'student.name'
                },
                {
                    data: 'maths',
                    name: 'maths'
                },
                {
                    data: 'science',
                    name: 'science'
                },
                {
                    data: 'history',
                    name: 'history'
                },
                {
                    data: 'term',
                    name: 'term'
                },
                {
                    data: 'total_marks',
                    name: 'total_marks'
                },
                {
                    data: 'created_on',
                    name: 'created_on'
                },
                {
                    data: 'action',
                    name: 'action',
                    searchable: false,
                    orderable: false,
                },
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $("td:first", nRow).html(iDisplayIndex + 1);
                return nRow;
            },
        });
    });
</script>

@endsection