@extends('layouts.app')

@section('section-name', 'Enter Student Marks')

@section('content')
<div class="mdc-layout-grid">
    <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-12-desktop">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{route('student-marks.store')}}" method="post" id="enterMarks" class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-desktop">
                <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
                    <p style="font-size: 15px; float: right; color:rgba(94, 94, 94, 0.87);"></p>
                    {{csrf_field()}}
                </div>
                <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-12-desktop">
                    <div class="mdc-card">
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
                            <p style="font-size: 15px; float: right; color:rgba(94, 94, 94, 0.87);">Enter Student Marks</p>
                        </div>
                        <br>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12-desktop">
                            <div class="mdc-text-field mdc-text-field--outlined ">
                                <select class="mdc-text-field__input" name="student" id="student" required>
                                    <option value=" ">Select</option>
                                    @foreach(\App\Models\Students::get() as $student)
                                    <option value="{{$student->id}}" @if(old('student')==$student->id) selected @endif>{{$student->name}}</option>
                                    @endforeach
                                </select>
                                <div class="mdc-notched-outline">
                                    <div class="mdc-notched-outline__leading"></div>
                                    <div class="mdc-notched-outline__notch">
                                        <label for="text-field-hero-input" class="mdc-floating-label">Select Student</label>
                                    </div>
                                    <div class="mdc-notched-outline__trailing"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12-desktop">
                            <div class="mdc-text-field mdc-text-field--outlined ">
                                <select class="mdc-text-field__input" name="term" id="term" required>
                                    <option value=" ">Select</option>
                                    <option value="One" @if(old('term')=="One" ) selected @endif>One</option>
                                    <option value="Two" @if(old('term')=="Two" ) selected @endif>Two</option>
                                </select>
                                <div class="mdc-notched-outline">
                                    <div class="mdc-notched-outline__leading"></div>
                                    <div class="mdc-notched-outline__notch">
                                        <label for="text-field-hero-input" class="mdc-floating-label">Select Term</label>
                                    </div>
                                    <div class="mdc-notched-outline__trailing"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12-desktop">
                            <div class="mdc-text-field mdc-text-field--outlined">
                                <input class="mdc-text-field__input" id="maths_mark" type="text" name="maths_mark" maxlength="2" value="{{ old('maths_mark') }}" required onkeypress="return isNumberKey(event)" />
                                <div class="mdc-notched-outline">
                                    <div class="mdc-notched-outline__leading"></div>
                                    <div class="mdc-notched-outline__notch">
                                        <label for="text-field-hero-input" class="mdc-floating-label">Enter Maths Mark</label>
                                    </div>
                                    <div class="mdc-notched-outline__trailing"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12-desktop">
                            <div class="mdc-text-field mdc-text-field--outlined">
                                <input class="mdc-text-field__input" id="science_mark" type="text" name="science_mark" maxlength="2" value="{{ old('science_mark') }}" required onkeypress="return isNumberKey(event)" />
                                <div class="mdc-notched-outline">
                                    <div class="mdc-notched-outline__leading"></div>
                                    <div class="mdc-notched-outline__notch">
                                        <label for="text-field-hero-input" class="mdc-floating-label">Enter Science Mark</label>
                                    </div>
                                    <div class="mdc-notched-outline__trailing"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12-desktop">
                            <div class="mdc-text-field mdc-text-field--outlined">
                                <input class="mdc-text-field__input" id="history_mark" type="text" name="history_mark" maxlength="2" value="{{ old('history_mark') }}" required onkeypress="return isNumberKey(event)" />
                                <div class="mdc-notched-outline">
                                    <div class="mdc-notched-outline__leading"></div>
                                    <div class="mdc-notched-outline__notch">
                                        <label for="text-field-hero-input" class="mdc-floating-label">Enter History Mark</label>
                                    </div>
                                    <div class="mdc-notched-outline__trailing"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
                            <button type="submit" class="mdc-button mdc-button--raised w-100">Add Student</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('append-assets')
<script>
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
@endsection